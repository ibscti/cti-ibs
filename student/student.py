from osv import osv, fields
class student(osv.Model):
    _name       = 'student'

    def score(self,cr,uid,ids,field_name = "",args = [],context = None):
        calc_avg = {}
        for etudiant in self.browse(cr,uid,ids):
            calc_avg[etudiant.id] = ( etudiant.score_eng + etudiant.score_web + etudiant.score_java  + etudiant.score_math ) / 4
        
        return calc_avg

    def passorno(self,cr,uid,ids,field_name = "",args = [],context = None):
        passno = {}
        for etudiant in self.browse(cr,uid,ids):
            if etudiant.score_avg < 10:
            	passno[etudiant.id] = "non valide"
            else:
            	if etudiant.score_avg >= 10 and etudiant.score_avg < 12:
            		passno[etudiant.id] = "valide avec mension passable!"
            	if etudiant.score_avg >= 12 and etudiant.score_avg < 14:
            		passno[etudiant.id] = "valide avec mension assez bien!"
            	if etudiant.score_avg >= 14 and etudiant.score_avg < 16:
            		passno[etudiant.id] = "valide avec mension bien!"
            	if etudiant.score_avg >= 16 and etudiant.score_avg < 18:
            		passno[etudiant.id] = "valide avec mension tres bien!"
            	if etudiant.score_avg >= 18:
            		passno[etudiant.id] = "valide avec mension excellent!"
        
        return passno

    _columns    = {
        'name'                : fields.char('Name', required=True),
        'email'               : fields.char('Email', required=True),
        'sex'                 : fields.selection([('male','Male'),('female','Female')],'Sex',readonly=False),
        'code'                : fields.char('University Code', required=True),
        'reg_date'            : fields.date('Registration date', required=True,),
        'score_java'          : fields.float('Java Score ', digits=(2,1)),
        'score_math'          : fields.float('Math Score',digits=(2,1)),
        'score_web'           : fields.float('Web Score',digits=(2,1)),
        'score_eng'           : fields.float('English Score',digits=(2,1)),
        'score_avg'           : fields.function(score, string='average', type='float', store=False),
        'desc'                : fields.function(passorno, string='Decision', type='char', store=False),
        'state'               : fields.many2one('res.country', 'Country', required=True),
        'active'              : fields.boolean('Statut'),
        'picture'             : fields.binary('Picture', filters='*.png,*.gif'),
    }
    _defaults   = { 
        'score_java'  : 0,
        'score_math'  : 0,
        'score_web'   : 0,
        'score_eng'   : 0,
        'active'      : True,
        'sex'		  :	'male',
    }
    
    _sql_constraints = [('name_uniq','unique(code)', 'Students code must be unique!')]


