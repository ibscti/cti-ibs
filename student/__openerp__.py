{ 
  'name' : 'Student',
  'version' : '1.0',
  'author' : 'Amr & Zakaria',
  'description' : 'Students scores manager module',
  'category': 'test',
  'depends' : [ "base" ],
  'sequence': 3,
  'data' : [
    # 'security/groups.xml',              # always load groups first! 
    # 'security/ir.model.access.csv',     # load access rights after groups 
    'student_view.xml', 
  ],  
  #'demo': ['demo/demo.xml'],              # demo data (for unit tests)
} 